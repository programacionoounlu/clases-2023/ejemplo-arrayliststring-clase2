package ar.edu.unlu.poo.clase2;

import java.util.ArrayList;
import java.util.ListIterator;

public class Ejercicio2 {
    public void ejecutarEjemplo() {
        String cadena = "Cadena Ejemplo";
        ArrayList<String> listaDeCadenas = new ArrayList<>();
        listaDeCadenas.add("Cadena 1");
        listaDeCadenas.add("Cadena 2");
        listaDeCadenas.add("Cadena 3");
        listaDeCadenas.add("Cadena Ejemplo");
        listaDeCadenas.add("Cadena 4");
        listaDeCadenas.add("Cadena Ejemplo");
        int resultado = ejercicio2(cadena, listaDeCadenas);
        System.out.println("Repeticiones: " + resultado);
    }

    public int ejercicio2(String unString, ArrayList<String> unaListaDeStrings) {
        int repeticiones = 0;
        // Estrategia de iteración 3
        ListIterator<String> iterador = unaListaDeStrings.listIterator();
        while (iterador.hasNext()) {
            String siguienteString = iterador.next();
            if (unString.equals(siguienteString)) {
                repeticiones++;
            }
        }
        return repeticiones;
    }
}
