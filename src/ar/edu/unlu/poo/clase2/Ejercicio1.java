package ar.edu.unlu.poo.clase2;

import java.util.ArrayList;

public class Ejercicio1 {
    public void ejecutarEjemplo() {
        String cadena = "Cadena Ejemplo";
        ArrayList<String> listaDeCadenas = new ArrayList<>();
        listaDeCadenas.add("Cadena 1");
        listaDeCadenas.add("Cadena 2");
        listaDeCadenas.add("Cadena 3");
        listaDeCadenas.add("Cadena Ejemplo");
        listaDeCadenas.add("Cadena 4");
        listaDeCadenas.add("Cadena 5");
        String resultado = ejercicio1(cadena, listaDeCadenas);
        System.out.println("Resultado: " + resultado);
    }

    public String ejercicio1(String unString, ArrayList<String> unaListaDeStrings) {
        // Estrategia de iteración 1
        for (int i = 0; i < unaListaDeStrings.size(); i++) {
            String siguienteString = unaListaDeStrings.get(i);
            if (unString.equals(siguienteString)) {
                return siguienteString;
            }
        }
        
        // Estrategia de iteración 2
        /*for (String siguienteString :
                unaListaDeStrings) {
            if (unString.equals(siguienteString)) {
                return siguienteString;
            }
        }*/
        return "<Nada encontrado>";
    }
}
