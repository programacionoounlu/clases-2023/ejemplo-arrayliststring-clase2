package ar.edu.unlu.poo.clase2;

public class EjemplosStrings {
    public static void main(String[] args) {
        System.out.println("Ejercicio 1: Crear un método que recibe 2 parámetros. En uno recibe un String y en otro un ArrayList<String>. Retornar la existencia del objeto String dentro del ArrayList.");

        Ejercicio1 ejercicio1 = new Ejercicio1();
        ejercicio1.ejecutarEjemplo();

        Ejercicio2 ejercicio2 = new Ejercicio2();
        ejercicio2.ejecutarEjemplo();
    }
}
